package io.gitlab.ettotog.codegen.observable.properties;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.gitlab.ettotog.codegen.api.Named;
import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Data
public class ObservablePropertiesSpec {
    @Data
    static class ObservableField implements Named {
        private String name;
        private String type;
        @JsonProperty("default")
        private Optional<String> defaultValue = Optional.empty();

        public String setMethod() {
            return "set" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
        }

    }

    private String name;
    @JsonProperty("package")
    private String pack;
    private List<ObservableField> fields = Collections.emptyList();
    private List<String> interfaces = Collections.emptyList();
    private Optional<String> parent = Optional.empty();
}
