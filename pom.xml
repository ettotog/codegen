<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>io.gitlab.ettotog.codegen</groupId>
    <artifactId>parent</artifactId>
    <version>0.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <properties>

        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <site.url>https://gitlab.com/ettotog/codegen</site.url>
        <maven-compiler.version>3.11.0</maven-compiler.version>
        <maven-plugin.version>3.9.0</maven-plugin.version>
        <maven-plugin-annotations.version>3.9.0</maven-plugin-annotations.version>

        <lombok.version>1.18.28</lombok.version>
        <jackson.version>2.15.2</jackson.version>
        <junit.version>5.10.0</junit.version>
        <assertj.version>3.9.1</assertj.version>
        <mockito-junit.version>4.4.0</mockito-junit.version>
        <plugin.compiler.version>3.5.1</plugin.compiler.version>
        <plugin.surefire.version>2.22.1</plugin.surefire.version>
        <plugin.jacoco.version>0.8.2</plugin.jacoco.version>
        <auto-service.version>1.1.1</auto-service.version>
        <javapoet.version>1.13.0</javapoet.version>
        <skipSources>true</skipSources>
        <skipJavadocs>true</skipJavadocs>

    </properties>

    <name>codegen</name>
    <description>Exploring code generation</description>
    <url>${site.url}</url>

    <licenses>
        <license>
            <name>GNU General Public License version 3</name>
            <url>https://opensource.org/licenses/GPL-3.0</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Ettore Leandro Tognoli</name>
            <email>ettore.leandro.tognoli@gmail.com</email>
        </developer>
    </developers>

    <scm>
        <connection>scm:git:git://gitlab.com/ettoreleandrotognoli/codegen.git</connection>
        <developerConnection>scm:git:ssh://gitlab.com:ettoreleandrotognoli/codegen.git</developerConnection>
        <url>https://gitlab.com/ettoreleandrotognoli/codegen/tree/main</url>
    </scm>

    <modules>
        <module>api</module>
        <module>engine</module>
        <module>codegen-data-plugin</module>
        <module>examples</module>
        <module>codegen-maven-plugin</module>
        <module>observable-properties</module>
        <module>observable-properties-plugin</module>
        <module>jackson-plugin</module>
        <module>predicate</module>
        <module>predicate-plugin</module>
    </modules>

    <dependencies>

        <dependency>
            <groupId>io.reactivex.rxjava3</groupId>
            <artifactId>rxjava</artifactId>
            <version>3.1.2</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <version>${mockito-junit.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>

            <!-- modules -->

            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>api</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>codegen-data-plugin</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>observable-properties</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>observable-properties-plugin</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>predicate</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>predicate-plugin</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>jackson-plugin</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>engine</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitlab.ettotog.codegen</groupId>
                <artifactId>codegen-maven-plugin</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- libraries -->

            <dependency>
                <groupId>com.squareup</groupId>
                <artifactId>javapoet</artifactId>
                <version>${javapoet.version}</version>
            </dependency>

            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
            </dependency>

            <!-- processor libraries -->

            <dependency>
                <groupId>com.google.auto.service</groupId>
                <artifactId>auto-service-annotations</artifactId>
                <version>${auto-service.version}</version>
            </dependency>

            <!-- maven plugin libraries -->

            <dependency>
                <groupId>org.apache.maven</groupId>
                <artifactId>maven-plugin-api</artifactId>
                <version>3.6.3</version>
            </dependency>

            <dependency>
                <groupId>org.apache.maven</groupId>
                <artifactId>maven-project</artifactId>
                <version>2.2.1</version>
            </dependency>

            <dependency>
                <groupId>org.apache.maven.plugin-tools</groupId>
                <artifactId>maven-plugin-annotations</artifactId>
                <version>${maven-plugin-annotations.version}</version>
            </dependency>

            <!-- jackson -->

            <dependency>
                <groupId>com.fasterxml.jackson.dataformat</groupId>
                <artifactId>jackson-dataformat-yaml</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.datatype</groupId>
                <artifactId>jackson-datatype-jsr310</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-core</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.datatype</groupId>
                <artifactId>jackson-datatype-jdk8</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-annotations</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <!-- test libraries -->

            <dependency>
                <groupId>org.junit</groupId>
                <artifactId>junit-bom</artifactId>
                <version>${junit.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.assertj</groupId>
                <artifactId>assertj-core</artifactId>
                <version>3.16.0</version>
                <scope>test</scope>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>3.4.0</version>
                <executions>
                    <execution>
                        <id>enforce-maven</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>3.6</version>
                                </requireMavenVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler.version}</version>
                <configuration>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                            <version>${lombok.version}</version>
                        </path>
                        <path>
                            <groupId>com.google.auto.service</groupId>
                            <artifactId>auto-service</artifactId>
                            <version>${auto-service.version}</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.2</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.2.1</version>
                <configuration>
                    <skipSource>${skipSources}</skipSource>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.3.2</version>
                <configuration>
                    <skip>${skipJavadocs}</skip>
                    <show>private</show>
                    <nohelp>false</nohelp>
                    <links>
                        <link>https://square.github.io/javapoet/javadoc/javapoet/</link>
                    </links>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <phase>package</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.8.2</version>
                <executions>
                    <execution>
                        <id>deploy</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>deploy</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${plugin.jacoco.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.3.2</version>
                <configuration>
                    <show>protected</show>
                </configuration>
                <reportSets>
                    <reportSet>
                        <id>aggregate</id>
                        <inherited>false</inherited>
                        <reports>
                            <report>aggregate</report>
                        </reports>
                    </reportSet>
                    <reportSet>
                        <id>default</id>
                        <reports>
                            <report>javadoc</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
        </plugins>
    </reporting>

    <profiles>
        <profile>
            <id>release</id>
            <properties>
                <release>true</release>
                <skipSources>false</skipSources>
                <skipJavadocs>false</skipJavadocs>
            </properties>
        </profile>
        <profile>
            <id>gitlab</id>
            <properties>
                <!--suppress UnresolvedMavenProperty -->
                <site.url>https://${CI_PROJECT_NAMESPACE}.${CI_PAGES_DOMAIN}/${CI_PROJECT_NAME}</site.url>
            </properties>
            <activation>
                <property>
                    <name>env.GITLAB_CI</name>
                    <value>true</value>
                </property>
            </activation>
            <repositories>
                <repository>
                    <id>gitlab-maven</id>
                    <!--suppress UnresolvedMavenProperty -->
                    <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
                </repository>
            </repositories>
            <distributionManagement>
                <repository>
                    <id>gitlab-maven</id>
                    <!--suppress UnresolvedMavenProperty -->
                    <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
                </repository>
                <snapshotRepository>
                    <id>gitlab-maven</id>
                    <!--suppress UnresolvedMavenProperty -->
                    <url>${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/maven</url>
                </snapshotRepository>
            </distributionManagement>
        </profile>
    </profiles>
</project>