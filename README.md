# CODEGEN

Just an idea to create code generators for any one who is tired to write boilerplate code in java.
I know that lombok is awesome, and I'm already using it, but we have more boilerplate code than getters, setters,
builders,
and so on.
I think sometimes we have specific boilerplate code for each system, so what do you think about create your own plugin
that generates your specific boilerplate code?

<https://ettotog.gitlab.io/codegen/site/apidocs/>

## Codegen Data Plugin

I miss the word `const` from C++,
and found very nice to use interfaces that expose only the getters to have a similar effect.
I think I copied this idea from kotlin.

So the data plugin generates your data classes with an immutable interface, a mutable interface,
and specific implementations for each one.

```yaml
- package: org.etto
  codegen:
    DataClass:
- name: Person
  fields:
    - name: name
      type: String
```

This yaml will generate this java code:

```java
package org.etto;

import java.util.Objects;

public interface Person {
    String PROP_NAME = "name";

    String getName();

    default Immutable asImmutable() {
        return new Immutable(this);
    }

    default Mutable asMutable() {
        return new DTO(this);
    }

    class Immutable implements Person {
        private final String name;

        public Immutable(Person other) {
            this.name = other.getName();
        }

        @Override
        public String getName() {
            return name;
        }

        public static Immutable of(Person source) {
            return new Immutable(source);
        }

        @Override
        public Immutable asImmutable() {
            return this;
        }

        @Override
        public Immutable clone() {
            return this;
        }

        @Override
        public String toString() {
            return String.format("org.etto.Person.Immutable { name=%s }", getName());
        }

        public boolean equals(Object other) {
            if (this == other) return true;
            if (Objects.isNull(other)) return false;
            if (!(other instanceof Person)) return false;
            Person person = (Person) other;
            if (!Objects.equals(getName(), person.getName())) return false;
            return true;
        }

        public int hashCode() {
            return Objects.hash(name);
        }
    }

    interface Mutable extends Person {
        default Mutable asMutable() {
            return this;
        }

        void setName(String name);

        Mutable name(String name);
    }

    class DTO implements Mutable {
        private String name;

        public DTO() {
        }

        public DTO(Person other) {
            this.name = other.getName();
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public DTO name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public DTO asMutable() {
            return this;
        }

        public DTO clone() {
            return new DTO(this);
        }

        public DTO shallowCopy(Person other) {
            this.name = other.getName();
            return this;
        }

        @Override
        public String toString() {
            return String.format("org.etto.Person.DTO { name=%s }", getName());
        }

        public boolean equals(Object other) {
            if (this == other) return true;
            if (Objects.isNull(other)) return false;
            if (!(other instanceof Person)) return false;
            Person person = (Person) other;
            if (!Objects.equals(getName(), person.getName())) return false;
            return true;
        }

        public int hashCode() {
            return Objects.hash(name);
        }
    }
}

```

## Codegen Jackson Plugin

If you are using jackson to serialize or unserialize your data,
I think you want to keep using the immutable interface sometimes,
but jackson needs to know the concrete class to unserialize.

```yaml
- package: org.etto
  codegen:
    DataClass:
    Jackson:
- name: Person
  fields:
    - name: name
      type: String
```

```java
package org.etto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Objects;

@JsonDeserialize(
        as = Person.DTO.class
)
public interface Person {
    /**
     * Same code of the previous example
     */
}
```

## Codegen Observable Plugin

This one is for who already worked with `PropertyChangeListener`, and `ObservableCollections`.
I used it a lot when I was learning swing and making some user interfaces on netbeans.

```yaml
- package: org.etto
  codegen:
    DataClass:
    ObservableProperties:
- name: Person
  fields:
    - name: name
      type: String
```

The `ObservableProperties` depends on `DataClass`.

```java
package org.etto;

import io.gitlab.ettotog.codegen.observable.properties.ObservableProperties;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Objects;

public interface Person {

    /**
     * Same code from the previous examples
     */
    class Observable implements Mutable, ObservableProperties {
        private final transient PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

        private String name;

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void setName(String name) {
            String oldName = this.name;
            this.name = name;
            if (!Objects.equals(oldName, name))
                propertyChangeSupport.firePropertyChange(Person.PROP_NAME, oldName, name);
        }

        @Override
        public Observable name(String name) {
            setName(name);
            return this;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }

        @Override
        public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
        }

        @Override
        public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
        }

        @Override
        public PropertyChangeListener[] getPropertyChangeListeners() {
            return propertyChangeSupport.getPropertyChangeListeners();
        }

        @Override
        public PropertyChangeListener[] getPropertyChangeListeners(String propertyName) {
            return propertyChangeSupport.getPropertyChangeListeners(propertyName);
        }

        @Override
        public Observable asObservable() {
            return this;
        }

        public static Observable of(Person source) {
            Observable person = new Observable();
            person.name = source.getName();
            return person;
        }

        @Override
        public String toString() {
            return String.format("org.etto.Person.Observable { name=%s }", getName());
        }

        public boolean equals(Object other) {
            if (this == other) return true;
            if (Objects.isNull(other)) return false;
            if (!(other instanceof Person)) return false;
            Person person = (Person) other;
            if (!Objects.equals(getName(), person.getName())) return false;
            return true;
        }
    }
}

```