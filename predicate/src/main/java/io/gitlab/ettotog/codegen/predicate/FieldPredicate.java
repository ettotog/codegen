package io.gitlab.ettotog.codegen.predicate;

import java.util.function.Predicate;

public interface FieldPredicate<M, T> extends Predicate<M> {
}
