package io.gitlab.ettotog.codegen.predicate;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PredicateSpec {

    private String name;
    @JsonProperty("package")
    private String pack;
}
